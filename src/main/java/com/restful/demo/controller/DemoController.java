package com.restful.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restful.demo.bean.Persona;

@RestController
@RequestMapping("/demo")
public class DemoController {
	
	@RequestMapping(value="/person", method = RequestMethod.GET)
	public Persona obtenerPersona(HttpServletRequest request) {
		Persona person = new Persona();
		person.setNombre("Peter");
		person.setApellidos("Parker");
		person.setDocumento("1234");
		
		return person;
	}

}
